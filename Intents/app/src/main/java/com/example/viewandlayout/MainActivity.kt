package com.example.viewandlayout

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val tag = "MainActivity"

    private var form: FormState = FormState(false, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: form
        }

        anrButton.setOnClickListener {
            Thread.sleep(11000)
        }

        DebugLogger.d(tag, "OnCreate")

        imageLogin(this)

        loginButton.setOnClickListener {
            val activityClass = SecondActivity::class.java
            val startActivityIntent = Intent(
                this,
                activityClass
            )

            val progressToAdd = ProgressBar(this).apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
                visibility = View.VISIBLE
            }

            val emailAdress = loginInput.text.toString()
            val isEmailValid: Boolean = Patterns.EMAIL_ADDRESS.matcher(emailAdress).matches()

            if (isEmailValid) {
                checkError()
                loginLayout.addView(progressToAdd)
                progressToAdd.visibility = View.VISIBLE
                checkLicense.isEnabled = false
                loginButton.isEnabled = false
                enterPassword.isEnabled = false
                loginInput.isEnabled = false
                textError.isVisible = true
                if (enterPassword.text.isEmpty() || loginInput.text.isEmpty()) {
                    Handler().postDelayed({
                        loginLayout.removeView(progressToAdd)
                        toast("Вход НЕ выполнен")
                        checkLicense.isEnabled = true
                        checkLicense.isChecked = false
                        loginButton.isEnabled = true
                        enterPassword.text.clear()
                        enterPassword.isEnabled = true
                        loginInput.text.clear()
                        loginInput.isEnabled = true
                        textError.isVisible = false
                    }, 2000)
                } else {
                    Handler().postDelayed({
                        loginLayout.removeView(progressToAdd)
                        toast("Вход выполнен")
                        checkLicense.isEnabled = true
                        checkLicense.isChecked = false
                        loginButton.isEnabled = false
                        enterPassword.text.clear()
                        enterPassword.isEnabled = false
                        loginInput.text.clear()
                        loginInput.isEnabled = false
                        textError.isVisible = false
                        startActivity(startActivityIntent)
                    }, 2000)

                }
            } else toast("Логин введен неверно")


        }
        enterPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                checkEditText(s)
            }
        })

        loginInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                checkEditText(s)
            }
        })
    }

    private fun toast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        DebugLogger.d(tag, "OnStart")
    }

    override fun onResume() {
        super.onResume()
        DebugLogger.d(tag, "OnResume")
        Log.v(tag, "Verbose")
    }

    override fun onPause() {
        super.onPause()
        DebugLogger.d(tag, "OnPause")
        Log.e(tag, "Error")
    }

    override fun onStop() {
        super.onStop()
        DebugLogger.d(tag, "OnStop")
        Log.i(tag, "Info")
    }

    override fun onDestroy() {
        super.onDestroy()
        DebugLogger.d(tag, "OnDestroy")
        Log.println(Log.ASSERT, tag, "Assert")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_COUNTER, form)
        form.valid = loginButton.isEnabled
        form.message = textError.text.toString()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: error("")
        loginButton.isEnabled = form.valid
        textError.text = form.message
    }

    private fun checkEditText(s: Editable?) {                                                       //проверяем введеные поля
        loginButton.isEnabled = s?.let { it.isNotEmpty() }
            ?: false                                                                                //активируем кнопку после ввода логина
    }

    private fun checkError() {
        if (enterPassword.text.isEmpty() || loginInput.text.isEmpty()) {
            textError.text = "Ошибка"
            loginButton.isEnabled = false
        } else {
            textError.setTextColor(resources.getColor(R.color.purple_700))
            textError.text = "Вход выполнен успешно"
            loginButton.isEnabled = true
        }
    }

    private fun imageLogin(context: Context) {
        Glide
            .with(context)
            .load("https://catalog.livestreetcms.com/uploads/images/00/05/80/2014/12/01/avatar_addon_11cb4_180x180.png")
            .into(imageView)
    }

    object DebugLogger {
        fun d(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.d(tag, message)
            }
        }
    }

    companion object {
        private const val KEY_COUNTER = "counter"
    }
}
