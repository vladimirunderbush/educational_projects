package com.example.viewandlayout

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_deeplink.*

class DeepLink: AppCompatActivity(R.layout.activity_deeplink) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleIntentData()
    }
//    http://student_name.com/info
    private fun handleIntentData() {
        intent.data?.lastPathSegment?.let { deepLink ->
            deepLinkTextView.text = deepLink
        }
    }
}