package com.example.viewandlayout

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*
import java.security.AccessControlContext

class SecondActivity : AppCompatActivity(R.layout.activity_second) {

    @SuppressLint("QueryPermissionsNeeded")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

//        registerForActivityResult(ActivityResultContracts.Dial)

        phoneNumberEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                checkEditText(s)
            }
        })
        callButton.setOnClickListener() {
            val phoneNumber = phoneNumberEdit.text.toString()
            val isPhoneNumberValid: Boolean = Patterns.PHONE.matcher(phoneNumber).matches()

            if (!isPhoneNumberValid) {
                toast("Номер телефоона введен некорректно")
            } else {
                val phoneIntent = Intent(Intent.ACTION_DIAL).apply {
                    data = Uri.parse("tel:$phoneNumber")
                }
                if (phoneIntent.resolveActivity(packageManager) != null) {
                    toast("No component to handle intent")
                } else {
                    startActivity(phoneIntent)
                    finish()
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            toast("Result OK")
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    private val callaActivityResultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                toast("RESULT FROM ACTION_DIAL")
            }
        }

//    if (intent.resolveActivity(packageManager) != null) {
//        callaActivityResultLauncher.launch(intent) }

    private fun checkEditText(s: Editable?) {                                                       //проверяем введеные поля
        callButton.isEnabled = s?.let { it.isNotEmpty() }
            ?: false                                                                                //активируем кнопку после ввода логина
    }

    private fun toast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    companion object {
        private const val KEY_NUMBER = "number key"

        fun getIntent(context: Context, number: Number): Intent {
            return Intent(context, SecondActivity::class.java)
                .putExtra(KEY_NUMBER, number)
        }
    }
}