package com.example.viewandlayout

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment(R.layout.fragment_login) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private val getTag = "MainActivity"

    private var form: FormState = FormState(false, "")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: form
        }

        anrButton?.setOnClickListener {
            Thread.sleep(11000)
        }


        DebugLogger.d(getTag, "OnCreate")

//        imageLogin(this)

        loginButton?.setOnClickListener {
            checkError()


//            val progressToAdd = ProgressBar(activity).apply {
//                layoutParams = LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.MATCH_PARENT
//                )
//                visibility = View.VISIBLE
//            }
//
//            loginLayout?.addView(progressToAdd)
//            progressToAdd.visibility = View.VISIBLE
//            checkLicense?.isEnabled = false
//            loginButton?.isEnabled = false
//            enterPassword?.text?.clear()
//            enterPassword?.isEnabled = false
//            loginInput?.text?.clear()
//            loginInput?.isEnabled = false
//            Handler().postDelayed({
//                if (enterPassword?.text?.isEmpty() == true || loginInput?.text?.isEmpty() == true) {
//                    loginLayout?.removeView(progressToAdd)
////                    Toast.makeText(requireActivity(), "Вход НЕ выполнен", Toast.LENGTH_LONG).show()
//                    toast("Вход НЕ выполнен")
//                    checkLicense?.isEnabled = true
//                    checkLicense?.isChecked = false
//                    loginButton?.isEnabled = false
//                    enterPassword?.isEnabled = true
////                    loginInput.isEnabled = true
//                    textError?.isVisible = false
//                } else {
//                    loginLayout?.removeView(progressToAdd)
//                    Toast.makeText(this.context, "Вход выполнен", Toast.LENGTH_LONG).show()
//                    toast("Вход выполнен")
//                    checkLicense?.isEnabled = true
//                    checkLicense?.isChecked = false
//                    loginButton?.isEnabled = false
//                    enterPassword?.isEnabled = true
////                    loginInput.isEnabled = true
//                }
//            }, 2000)

        }
        enterPassword?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                checkEditText(s)
            }
        })

        loginInput?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                checkEditText(s)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        DebugLogger.d(getTag, "OnStart")
    }

    override fun onResume() {
        super.onResume()
        DebugLogger.d(getTag, "OnResume")
        Log.v(tag, "Verbose")
    }

    override fun onPause() {
        super.onPause()
        DebugLogger.d(getTag, "OnPause")
        Log.e(tag, "Error")
    }

    override fun onStop() {
        super.onStop()
        DebugLogger.d(getTag, "OnStop")
        Log.i(tag, "Info")
    }

    override fun onDestroy() {
        super.onDestroy()
        DebugLogger.d(getTag, "OnDestroy")
        Log.println(Log.ASSERT, tag, "Assert")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_COUNTER, form)
        form.valid = loginButton?.isEnabled == true
        form.message = textError?.text.toString()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: error("")
        }
        loginButton?.isEnabled = form.valid
        textError?.text = form.message
    }

    fun checkEditText(s: Editable?) {                                                       //проверяем введеные поля
        loginButton?.isEnabled = s?.isNotEmpty()
            ?: false                                                                                //активируем кнопку после ввода логина
    }

    private fun checkError() {
        if (enterPassword?.text?.isEmpty() == true || loginInput?.text?.isEmpty() == true) {
            textError?.text = "Ошибка"
            loginButton?.isEnabled = false
        } else {
            textError?.setTextColor(resources.getColor(R.color.purple_700))
            textError?.text = "Вход выполнен успешно"
            loginButton?.isEnabled = true
            (activity as? TransferFragment)?.transferToFragment(MainFragment())
        }
    }

//    private fun imageLogin(context: LoginFragment) {
//        Glide
//            .with(context)
//            .load("https://catalog.livestreetcms.com/uploads/images/00/05/80/2014/12/01/avatar_addon_11cb4_180x180.png")
//            .into(imageView)
//    }

    object DebugLogger {
        fun d(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.d(tag, message)
            }
        }
    }

    companion object {
        private const val KEY_COUNTER = "counter"
    }

    private fun toast(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_LONG).show()
    }
}