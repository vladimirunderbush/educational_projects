package com.example.viewandlayout

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_list.*

class MainFragment : Fragment(R.layout.fragment_main), TransferFragment {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showListFragment()
    }

    override fun transferToFragment(fragment: Fragment) {
        childFragmentManager.beginTransaction()
            .replace(R.id.containerMain, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun showListFragment() {
        childFragmentManager.beginTransaction()
            .replace(R.id.containerMain, ListFragment())
            .addToBackStack(null)
            .commit()
    }
}