package com.example.viewandlayout

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.OnBackPressedCallback
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner

class ListFragment : Fragment(R.layout.fragment_list) {

    private val transferFragment: TransferFragment?
        get() = parentFragment?.let { it as TransferFragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.let { it as ViewGroup }
            .children
            .mapNotNull { it as? Button }
            .forEach { button ->
                button.setOnClickListener {
                    onTransferFragment(DetailFragment())
                }
            }
    }

    private fun onTransferFragment(fragment: Fragment) {
        transferFragment?.transferToFragment(fragment)
    }

    companion object {
        val KEY_TEXT = "text"
    }
}