package com.example.viewandlayout

import androidx.fragment.app.Fragment

interface TransferFragment {
    fun transferToFragment(fragment: Fragment) {
    }

    fun onItemSelect(text: String) {
    }
}