package com.example.viewandlayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.*


class MainActivity : AppCompatActivity(), TransferFragment {

//    private val tag = "MainActivity"
//
//    private var form: FormState = FormState(false, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showLoginFragment()
        if (isLogin) fragment is MainFragment
            else showLoginFragment()
    }

    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.container)?.childFragmentManager?.takeIf {
            it.backStackEntryCount > 1
        }?.popBackStack() ?: super.onBackPressed()
    }

    private fun showLoginFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, LoginFragment())
            .commitNow()
    }

    override fun transferToFragment(fragment: Fragment) {
        if (fragment is MainFragment) isLogin = true
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }
    companion object {
        private var isLogin = true
    }
}

//        if (savedInstanceState != null) {
//            form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: form
//        }
//
//        anrButton.setOnClickListener {
//            Thread.sleep(11000)
//        }
//
//        DebugLogger.d(tag, "OnCreate")
//
//        imageLogin(this)
//
//        loginButton.setOnClickListener {
////            loginInput.setText("")
////            enterPassword.setText("")
//            checkError()
//
//
//            val progressToAdd = ProgressBar(this).apply {
//                layoutParams = LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.MATCH_PARENT
//                )
//                visibility = View.VISIBLE
//            }
//
//            loginLayout.addView(progressToAdd)
//            progressToAdd.visibility = View.VISIBLE
//            checkLicense.isEnabled = false
//            loginButton.isEnabled = false
//            enterPassword.text.clear()
//            enterPassword.isEnabled = false
//            loginInput.text.clear()
//            loginInput.isEnabled = false
//            Handler().postDelayed({
//                if (enterPassword.text.isEmpty() || loginInput.text.isEmpty()) {
//                    loginLayout.removeView(progressToAdd)
//                    Toast.makeText(this, "Вход НЕ выполнен", Toast.LENGTH_LONG).show()
//                    checkLicense.isEnabled = true
//                    checkLicense.isChecked = false
//                    loginButton.isEnabled = false
//                    enterPassword.isEnabled = true
////                    loginInput.isEnabled = true
//                    textError.isVisible = false
//                } else {
//                    loginLayout.removeView(progressToAdd)
//                    Toast.makeText(this, "Вход выполнен", Toast.LENGTH_LONG).show()
//                    checkLicense.isEnabled = true
//                    checkLicense.isChecked = false
//                    loginButton.isEnabled = false
//                    enterPassword.isEnabled = true
////                    loginInput.isEnabled = true
//                }
//            }, 2000)
//
//        }
//        enterPassword.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//            override fun afterTextChanged(s: Editable?) {
//                checkEditText(s)
//            }
//        })
//
//        loginInput.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//            override fun afterTextChanged(s: Editable?) {
//                checkEditText(s)
//            }
//        })
//    }
//
//    override fun onStart() {
//        super.onStart()
//        DebugLogger.d(tag, "OnStart")
//    }
//
//    override fun onResume() {
//        super.onResume()
//        DebugLogger.d(tag, "OnResume")
//        Log.v(tag, "Verbose")
//    }
//
//    override fun onPause() {
//        super.onPause()
//        DebugLogger.d(tag, "OnPause")
//        Log.e(tag, "Error")
//    }
//
//    override fun onStop() {
//        super.onStop()
//        DebugLogger.d(tag, "OnStop")
//        Log.i(tag, "Info")
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        DebugLogger.d(tag, "OnDestroy")
//        Log.println(Log.ASSERT, tag, "Assert")
//    }
//
//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putParcelable(KEY_COUNTER, form)
//        form.valid = loginButton.isEnabled
//        form.message = textError.text.toString()
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        form = savedInstanceState.getParcelable<FormState>(KEY_COUNTER) ?: error("")
//        loginButton.isEnabled = form.valid
//        textError.text = form.message
//    }
//
//    private fun checkEditText(s: Editable?) {                                                       //проверяем введеные поля
//        loginButton.isEnabled = s?.let { it.isNotEmpty() }
//            ?: false                                                                                //активируем кнопку после ввода логина
//    }
//
//    private fun checkError() {
//        if (enterPassword.text.isEmpty() || loginInput.text.isEmpty()) {
//            textError.text = "Ошибка"
//            loginButton.isEnabled = false
//        } else {
//            textError.setTextColor(resources.getColor(R.color.purple_700))
//            textError.text = "Вход выполнен успешно"
//            loginButton.isEnabled = true
//        }
//    }
//
//    private fun imageLogin(context: Context) {
//        Glide
//            .with(context)
//            .load("https://catalog.livestreetcms.com/uploads/images/00/05/80/2014/12/01/avatar_addon_11cb4_180x180.png")
//            .into(imageView)
//    }
//
//    object DebugLogger {
//        fun d(tag: String, message: String) {
//            if (BuildConfig.DEBUG) {
//                Log.d(tag, message)
//            }
//        }
//    }
//
//    companion object {
//        private const val KEY_COUNTER = "counter"
//    }
//}
