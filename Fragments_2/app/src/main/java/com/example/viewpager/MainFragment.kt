package com.example.viewpager

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.io.Serializable

class MainFragment : Fragment(R.layout.fragment_main), InterfaceDialogAdapter {

    private var screens: List<Article> = listOf(
        Article(
            textTitleRes = R.string.titleName1,
            textArticleRes = R.string.articleText1,
            drawableRes = R.drawable.sport_1,
            tipeArticle = TipeArticle.sport
        ),
        Article(
            textTitleRes = R.string.titleName2,
            textArticleRes = R.string.articleText2,
            drawableRes = R.drawable.sport_2,
            tipeArticle = TipeArticle.sport
        ),
        Article(
            textTitleRes = R.string.titleName3,
            textArticleRes = R.string.articleText3,
            drawableRes = R.drawable.health_1,
            tipeArticle = TipeArticle.health
        ),
        Article(
            textTitleRes = R.string.titleName4,
            textArticleRes = R.string.articleText4,
            drawableRes = R.drawable.health_2,
            tipeArticle = TipeArticle.health
        ),
        Article(
            textTitleRes = R.string.titleName5,
            textArticleRes = R.string.articleText5,
            drawableRes = R.drawable.business_1,
            tipeArticle = TipeArticle.business
        ),
        Article(
            textTitleRes = R.string.titleName6,
            textArticleRes = R.string.articleText6,
            drawableRes = R.drawable.business_2,
            tipeArticle = TipeArticle.business
        ),
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val showChangeArticle = requireView().findViewById<Button>(R.id.showChangeArticle)
        showChangeArticle.setOnClickListener { showDialogFragment() }

        if (savedInstanceState == null) {

            val adapter = requireActivity().let { OnboardingAdapter(screens, it) }
            val viewPager = requireView().findViewById<ViewPager2>(R.id.viewPager)

            viewPager.adapter = adapter

            val tabLayout = requireView().findViewById<TabLayout>(R.id.tabLayout)
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.setText(screens[position].textTitleRes)
            }.attach()

        } else {

            currentTipe = savedInstanceState.getSerializable(KEY_COUNTER) as TipeArticle?
            val filteredScreensSavedState = screens.filter { it.tipeArticle == currentTipe }

            val adapter = requireActivity().let { OnboardingAdapter(filteredScreensSavedState, it) }
            val viewPager = requireView().findViewById<ViewPager2>(R.id.viewPager)

            viewPager.adapter = adapter

            val tabLayout = requireView().findViewById<TabLayout>(R.id.tabLayout)

            TabLayoutMediator(
                tabLayout,
                viewPager
            )
            { tab, position -> tab.setText(filteredScreensSavedState[position].textTitleRes) }.attach()
        }
    }

    private fun showDialogFragment() {
        Dialog()
            .show(childFragmentManager, "Dialog")
    }

    var currentTipe: TipeArticle? = null
    override fun transferAdapter(tipeArticle: TipeArticle?) {
        currentTipe = tipeArticle
        val filteredScreens = screens.filter { it.tipeArticle == tipeArticle }
        val adapter = OnboardingAdapter(filteredScreens, requireActivity())
        val viewPager = requireView().findViewById<ViewPager2>(R.id.viewPager)

        viewPager.adapter = adapter

        val tabLayout = requireView().findViewById<TabLayout>(R.id.tabLayout)

        TabLayoutMediator(
            tabLayout,
            viewPager
        ) { tab, position -> tab.setText(filteredScreens[position].textTitleRes) }.attach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(KEY_COUNTER, currentTipe)
        super.onSaveInstanceState(outState)
    }


    private companion object {

        private const val KEY_COUNTER = "counter"
    }
}
