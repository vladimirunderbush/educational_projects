package com.example.viewpager

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class Dialog : DialogFragment(), InterfaceDialogAdapter {

    var selected: TipeArticle? = null;
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val tipeArticleDialog = TipeArticle.values().map { it.name }.toTypedArray()
        Log.d("setSingleChoiceItem", "which ${tipeArticleDialog}")

        return AlertDialog.Builder(requireActivity())
            .setTitle(R.string.changeArticleDialog)
            .setSingleChoiceItems(tipeArticleDialog, -1) { _, which ->
                selected = TipeArticle.values()[which]
                Log.d("setSingleChoiceItem", "which ${selected}")
            }
            .setPositiveButton("ok") { dialog, which ->
                selected?.let { (parentFragment as InterfaceDialogAdapter).transferAdapter(it) }
                savedInstanceState?.get("SingleChoice")
                Log.d("setSingleChoiceItem", "selected ${selected}")
            }
            .setNegativeButton("cancel", { _, _ -> })
            .create()
    }

    fun toast(text: String) {
        Toast.makeText(activity, "", Toast.LENGTH_SHORT).show()
    }

    override fun transferAdapter(tipeArticle: TipeArticle?) {
    }
}