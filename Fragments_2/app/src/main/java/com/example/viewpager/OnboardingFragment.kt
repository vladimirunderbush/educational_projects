package com.example.viewpager

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment

class OnboardingFragment: Fragment(R.layout.fragment_onboarding) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val titleArticle = requireView().findViewById<TextView>(R.id.titleArticle)
        titleArticle.setText(requireArguments().getInt(KEY_TEXT_TITLE))

        val textArticle = requireView().findViewById<TextView>(R.id.textArticle)
        textArticle.setText(requireArguments().getInt(KEY_TEXT_ARTICLE))

        val imageView = requireView().findViewById<ImageView>(R.id.imageView)
        imageView.setImageResource(requireArguments().getInt(KEY_IMAGE))

    }

    companion object {
        private const val KEY_TEXT_TITLE = "text"
        private const val KEY_TEXT_ARTICLE = "text2"
        private const val KEY_IMAGE = "image"

        fun newInstance(
            @StringRes textTitleRes: Int,
            @StringRes textArticleRes: Int,
            @DrawableRes drawableRes: Int

        ): OnboardingFragment {
            return OnboardingFragment().withArguments {
                putInt(KEY_TEXT_TITLE, textTitleRes)
                putInt(KEY_TEXT_ARTICLE, textArticleRes)
                putInt(KEY_IMAGE, drawableRes)
            }
        }
    }
}