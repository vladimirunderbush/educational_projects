package com.example.viewpager

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Article (
    @StringRes val textTitleRes: Int,
    @StringRes val textArticleRes: Int,
    @DrawableRes val drawableRes: Int,
    val tipeArticle: TipeArticle
    )
    enum class
    TipeArticle() {
        sport,
        business,
        health
    }
