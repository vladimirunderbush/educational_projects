package com.example.viewpager

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class OnboardingAdapter(
    private val screens: List<Article>,
    activity: FragmentActivity): FragmentStateAdapter(activity), InterfaceDialogAdapter {

    override fun getItemCount(): Int {
        return screens.size

    }

    override fun createFragment(position: Int): Fragment {
        val screen: Article = screens[position]
        return OnboardingFragment.newInstance(
            textTitleRes = screen.textTitleRes,
            textArticleRes = screen.textArticleRes,
            drawableRes = screen.drawableRes
        )
    }

    override fun transferAdapter(tipeArticle: TipeArticle?) {
    }
}