package com.example.listfragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListFragment : Fragment(R.layout.fragment_list), CreateItemList {

    var players: List<Players> = emptyList()

    var playerAdapter: PlayerAdapter? = null

    var currentPlayers: Players = Players()


    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pushFab()
        val emptyListTextView = view.findViewById<TextView>(R.id.emptyListTextView)


        if (savedInstanceState == null) {
            emptyListTextView.isVisible = true
            initList()
            playerAdapter?.updatePlayers(players)
            playerAdapter?.notifyItemInserted(0)
            Log.d("onCreateItem", "ListFragment0 ${players}")
//
        } else {
            players =
                savedInstanceState.getParcelableArrayList<Players>(KEY_COUNTER) as List<Players>
            initList()
            Log.d("onCreateItem", "ListFragmentElse ${players}")
            playerAdapter?.updatePlayers(players)
            playerAdapter?.notifyDataSetChanged()
            emptyListTextView.isVisible = players.size == 0
            Log.d("onCreateItem", "ListFragmentElse2 ${players}")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        playerAdapter = null
    }

    private fun pushFab() {
        val addFab = requireView().findViewById<FloatingActionButton>(R.id.addFab)
        addFab.setOnClickListener { showDialog() }
    }

    private fun initList() {
        Log.d("onCreateItem", "InitList ${players}")
        val listFragmentRecyclerView =
            requireView().findViewById<RecyclerView>(R.id.listFragmentRecyclerView)
        playerAdapter = PlayerAdapter { position -> deletePlayer(position) }
        with(listFragmentRecyclerView) {
            adapter = playerAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun deletePlayer(position: Int) {
        players =
            players.filterIndexed { index, players -> index != position } as ArrayList<Players>
        players.let { playerAdapter?.updatePlayers(it) }
        playerAdapter?.notifyItemRemoved(position)
        val emptyListTextView = view?.findViewById<TextView>(R.id.emptyListTextView)
        if (players.size == 0)
            emptyListTextView?.isVisible = true
    }

    private fun showDialog() {
        val emptyListTextView = view?.findViewById<TextView>(R.id.emptyListTextView)
        emptyListTextView?.isVisible = false
        Dialog()
            .show(childFragmentManager, "Dialog")
    }

    private companion object {
        private const val KEY_COUNTER = "counter"
    }

    @SuppressLint("WrongConstant")
    override fun onCreateItem(player: List<Players>) {

        Log.d("onCreateItem", "ListFragment6 ${player}")
        players = player + players

        Log.d("onCreateItem", "ListFragment4 ${players}")
        players.let { playerAdapter?.updatePlayers(it) }
        playerAdapter?.notifyItemInserted(0)
        val listFragmentRecyclerView =
            requireView().findViewById<RecyclerView>(R.id.listFragmentRecyclerView)
        listFragmentRecyclerView.scrollToPosition(0)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(KEY_COUNTER, ArrayList(players))
        Log.d("onCreateItem", "SaveInstanceState ${ArrayList(players)}")

    }

}


