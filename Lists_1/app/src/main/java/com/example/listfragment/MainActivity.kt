package com.example.listfragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val hasFragment = supportFragmentManager.findFragmentById(R.id.containerMain) != null // проверка на отсутствие фрагмента. ВАЖНО при восстановлении состояния!
        if(!hasFragment) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.containerMain, ListFragment())
                .commit()
        }
    }
}