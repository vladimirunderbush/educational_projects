package com.example.listfragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment


class Dialog : DialogFragment(), CreateItemList {

    var playersDialog: List<Players> = emptyList()

    @SuppressLint("WrongConstant")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = layoutInflater.inflate(R.layout.fragment_dialog, null)

        val enterNameEditText = view?.findViewById<EditText>(R.id.enterNameEditText)
        val enterAgeEditText = view?.findViewById<EditText>(R.id.enterAgeEditText)
        val enterGameEditText = view?.findViewById<EditText>(R.id.enterGameEditText)
        val enterLinkAvatar = view?.findViewById<EditText>(R.id.enterLinkAvatar)
        val genreGameEditText = view?.findViewById<EditText>(R.id.genreGameEditText)
        val cyberGamerCheckBox = view?.findViewById<CheckBox>(R.id.cyberGamerCheckBox)
        val onlinerCheckBox = view?.findViewById<CheckBox>(R.id.onlinerCheckBox)
        val gamercheckBox = view?.findViewById<CheckBox>(R.id.gamercheckBox)
        val singlePlayerCheckBox = view?.findViewById<CheckBox>(R.id.singlePlayerCheckBox)

        gamercheckBox?.setOnClickListener {
            if (gamercheckBox.isChecked) {
                genreGameEditText?.isVisible = true
                enterGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = false
                onlinerCheckBox?.isEnabled = false
                singlePlayerCheckBox?.isEnabled = false
            } else {
                enterGameEditText?.isVisible = false
                genreGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = true
                onlinerCheckBox?.isEnabled = true
                singlePlayerCheckBox?.isEnabled = true
            }
        }

        cyberGamerCheckBox?.setOnClickListener {
            if (cyberGamerCheckBox.isChecked) {
                enterGameEditText?.isVisible = true
                genreGameEditText?.isVisible = false
                gamercheckBox?.isEnabled = false
                onlinerCheckBox?.isEnabled = false
                singlePlayerCheckBox?.isEnabled = false
            } else {
                enterGameEditText?.isVisible = false
                genreGameEditText?.isVisible = false
                gamercheckBox?.isEnabled = true
                onlinerCheckBox?.isEnabled = true
                singlePlayerCheckBox?.isEnabled = true
            }
        }

        onlinerCheckBox?.setOnClickListener {
            if (onlinerCheckBox.isChecked) {
                enterGameEditText?.isVisible = true
                genreGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = false
                gamercheckBox?.isEnabled = false
                singlePlayerCheckBox?.isEnabled = false
            } else {
                enterGameEditText?.isVisible = false
                genreGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = true
                gamercheckBox?.isEnabled = true
                singlePlayerCheckBox?.isEnabled = true
            }
        }

        singlePlayerCheckBox?.setOnClickListener {
            if (singlePlayerCheckBox.isChecked) {
                enterGameEditText?.isVisible = true
                genreGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = false
                onlinerCheckBox?.isEnabled = false
                gamercheckBox?.isEnabled = false
            } else {
                enterGameEditText?.isVisible = false
                genreGameEditText?.isVisible = false
                cyberGamerCheckBox?.isEnabled = true
                onlinerCheckBox?.isEnabled = true
                gamercheckBox?.isEnabled = true
            }
        }

        return AlertDialog.Builder(requireActivity())
            .setView(view)
            .setPositiveButton("ok")
            { _, _ ->
                Log.d("setPositiveButton", "Dialog")

                if (gamercheckBox?.isChecked == true) {
                    playersDialog = arrayListOf(
                        Players.Gamer(
                            name = enterNameEditText?.text.toString(),
                            avatarLink = enterLinkAvatar?.text.toString(),
                            age = enterAgeEditText?.text.toString(),
                            gameGenre = genreGameEditText?.text.toString()

                        )
                    )
                } else
                    if (cyberGamerCheckBox?.isChecked == true) {
                        playersDialog = arrayListOf(
                            Players.CyberGemer(
                                name = enterNameEditText?.text.toString(),
                                avatarLink = enterLinkAvatar?.text.toString(),
                                age = enterAgeEditText?.text.toString(),
                                game = enterGameEditText?.text.toString()
                            )
                        )
                        Log.d("setPositiveButton", "CyberGemer")
                    } else
                        if (onlinerCheckBox?.isChecked == true) {
                            playersDialog = arrayListOf(
                                Players.Onliner(
                                    name = enterNameEditText?.text.toString(),
                                    avatarLink = enterLinkAvatar?.text.toString(),
                                    age = enterAgeEditText?.text.toString(),
                                    game = enterGameEditText?.text.toString()
                                )
                            )
                            Log.d("setPositiveButton", "Onliner")
                        } else
                            if (singlePlayerCheckBox?.isChecked == true) {
                                playersDialog = arrayListOf(
                                    Players.SinglePlayer(
                                        name = enterNameEditText?.text.toString(),
                                        avatarLink = enterLinkAvatar?.text.toString(),
                                        age = enterAgeEditText?.text.toString(),
                                        game = enterGameEditText?.text.toString()
                                    )
                                )
                                Log.d("setPositiveButton", "Single")
                            } else Toast.makeText(context, "You havan't added a gamer", 0).show()

                Log.d("onCreateItem", "Dialog1 ${playersDialog}")
                playersDialog.let {
                    if (it != null) {
                        (parentFragment as CreateItemList).onCreateItem(it)
                        Log.d("onCreateItem", "Dialog2 ${playersDialog}")
                    }
                }
            }
            .create()
    }

    override fun onCreateItem(player: List<Players>) {
    }
}