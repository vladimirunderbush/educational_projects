package com.example.listfragment

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class PlayerAdapter(
    private val onItemClicked: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var players: List<Players> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_GAMER -> GamerHolder(parent.inflate(R.layout.item_gamers), onItemClicked)
            TYPE_CYBERGAMER -> CyberGamerHolder(
                parent.inflate(R.layout.item_cybergemer),
                onItemClicked
            )
            TYPE_ONLINER -> OnlinerHolder(parent.inflate(R.layout.item_onliner), onItemClicked)
            TYPE_SINGLEPLAYER -> SinglePlayerHolder(
                parent.inflate(R.layout.item_singleplayer),
                onItemClicked
            )
            else -> error("Incorrect viewTipe=$viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
//
        return when (players[position]) {
            is Players.Gamer -> TYPE_GAMER
            is Players.CyberGemer -> TYPE_CYBERGAMER
            is Players.Onliner -> TYPE_ONLINER
            else  -> TYPE_SINGLEPLAYER
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is GamerHolder -> {
                val player = players[position].let { it as? Players.Gamer }
                    ?: error("Player at position = $position is not a gamer")
                holder.bind(player)
            }

            is CyberGamerHolder -> {
                val player = players[position].let { it as? Players.CyberGemer }
                    ?: error("Player at position = $position is not a cyber gamer")
                holder.bind(player)
            }

            is OnlinerHolder -> {
                val player = players[position].let { it as? Players.Onliner }
                    ?: error("Player at position = $position is not a Online gamer")
                holder.bind(player)
            }

            is SinglePlayerHolder -> {
                val player = players[position].let { it as? Players.SinglePlayer }
                    ?: error("Player at position = $position is not a Single player")
                holder.bind(player)
            }

            else -> error("Incorrect View holder = $holder")
        }
    }


    override fun getItemCount(): Int = players.size

    fun updatePlayers(newPlayers: List<Players>) {
        players = newPlayers

    }

    abstract class BasePlayerHolder(
        view: View,
        onItemClicked: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private var nameTextView: TextView = view.findViewById(R.id.nameTextView)
        private var ageTextView: TextView = view.findViewById(R.id.ageTextView)
        private var avatarImageView: ImageView = view.findViewById(R.id.avatarImageView)

        init {
            view.setOnClickListener {
                onItemClicked(bindingAdapterPosition)
            }
        }

        protected fun bindMainInfo(
            name: String,
            avatarLink: String,
            age: String,
        ) {
            nameTextView.text = name
            ageTextView.text = age

            Glide.with(itemView)
                .load(avatarLink)
                .apply(RequestOptions.errorOf(R.drawable.ic_error))
                .apply(RequestOptions.placeholderOf(R.drawable.ic_baseline_portrait))
                .into(avatarImageView)
        }
    }

    class GamerHolder(
        view: View,
        onItemClicked: (position: Int) -> Unit
    ) : BasePlayerHolder(view, onItemClicked) {

        private val gameGenre: TextView = view.findViewById(R.id.genreTextView)

        fun bind(player: Players.Gamer) {
            bindMainInfo(player.name, player.avatarLink, player.age)
            gameGenre.text = player.gameGenre
        }
    }

    class CyberGamerHolder(
        view: View,
        onItemClicked: (position: Int) -> Unit
    ) : BasePlayerHolder(view, onItemClicked) {

        private val enterGame: TextView = view.findViewById(R.id.nameGameTextView)

        fun bind(player: Players.CyberGemer) {
            bindMainInfo(player.name, player.avatarLink, player.age)
            enterGame.text = player.game

        }
    }

    class OnlinerHolder(
        view: View,
        onItemClicked: (position: Int) -> Unit
    ) : BasePlayerHolder(view, onItemClicked) {

        private val enterGame: TextView = view.findViewById(R.id.nameGameTextView)

        fun bind(player: Players.Onliner) {
            bindMainInfo(player.name, player.avatarLink, player.age)
            enterGame.text = player.game
        }
    }

    class SinglePlayerHolder(
        view: View,
        onItemClicked: (position: Int) -> Unit
    ) : BasePlayerHolder(view, onItemClicked) {

        private val enterGame: TextView = view.findViewById(R.id.nameGameTextView)

        fun bind(player: Players.SinglePlayer) {
            bindMainInfo(player.name, player.avatarLink, player.age)
            enterGame.text = player.game
        }
    }

    companion object {
        private const val TYPE_GAMER = 1
        private const val TYPE_CYBERGAMER = 2
        private const val TYPE_SINGLEPLAYER = 3
        private const val TYPE_ONLINER = 4
    }
}