package com.example.listfragment

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
open class Players : Parcelable {

    data class Gamer(
        val name: String,
        val age: String,
        val avatarLink: String,
        val gameGenre: String
    ): Players()

    data class Onliner(
        val name: String,
        val age: String,
        val avatarLink: String,
        val game: String
    ): Players()

    data class CyberGemer(
        val name: String,
        val age: String,
        val avatarLink: String,
        val game: String
    ): Players()

    data class SinglePlayer(
        val name: String,
        val age: String,
        val avatarLink: String,
        val game: String
    ): Players()
}
