package com.example.listfragment

data class User(
    val name: String,
    val age: String,
    val avatarLink: String,
    val isGamer: Boolean
)
